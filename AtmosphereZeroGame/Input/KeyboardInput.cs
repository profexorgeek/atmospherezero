﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlatRedBall.Input;
using Microsoft.Xna.Framework.Input;

namespace AtmosphereZeroGame.Input
{
    public class KeyboardInput : IShipInput
    {
        public I2DInput Direction { get; private set; } = InputManager.Keyboard.Get2DInput(Keys.A, Keys.D, Keys.W, Keys.S);
        public IPressableInput Firing { get; private set; } = InputManager.Keyboard.GetKey(Keys.Space);
    }
}
