﻿using FlatRedBall.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmosphereZeroGame.Input
{
    public interface IShipInput
    {
        I2DInput Direction { get; }
        IPressableInput Firing { get; }
    }
}
