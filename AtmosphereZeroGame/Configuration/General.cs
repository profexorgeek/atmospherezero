﻿using AtmosphereZeroGame.NetStates;
using RedGrin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmosphereZeroGame.Configuration
{
    public static class General
    {
        /// <summary>
        /// Network configuration setup
        /// </summary>
        public static NetworkConfiguration NetConfig { get; set; } = new NetworkConfiguration()
        {
            ApplicationName = "AtmosphereZero",
            ApplicationPort = 1337,
            DeadReckonSeconds = 3f,
            EntityStateTypes = new System.Collections.Generic.List<Type>()
            {
                typeof(ShipNetState),
                typeof(ShotNetState)
            },

#if DEBUG
            SimulatedDuplicateChance = 0,
            SimulatedLoss = 0,
            SimulatedMinimumLatencySeconds = 0,
            SimulatedRandomLatencySeconds = 0,
#endif

        };

        /// <summary>
        /// How often the server should perform a dead reckoning cycle
        /// </summary>
        public static float DeadReckonFreqSeconds { get; set; } = 1f;
    }
}
