using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using RedGrin;
using AtmosphereZeroGame.NetStates;
using AtmosphereZeroGame.Utilities;

namespace AtmosphereZeroGame.Entities
{
	public partial class Shot : INetworkEntity
	{
        public const float ShotLifeSeconds = 4f;
        public const float ShotSpeed = 500f;
        public const float ShotDamage = 50f;

        public long OwnerId { get; set; }
        public long EntityId { get; set; }

        private ShotNetState netState = new ShotNetState();
        private float shotLifeRemaining = 0;

		private void CustomInitialize()
		{


		}

		private void CustomActivity()
		{
            CheckShotDeath();

		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        public object GetState()
        {
            this.CopyToState(netState);
            netState.ShotLife = ShotLifeSeconds;
            return netState;
        }

        public void UpdateState(object entityState, double stateTime, bool isReckoningState = false)
        {
            netState = (ShotNetState)entityState;
            var deltaTime = (float)(Game1.Network.ServerTime - stateTime);

            if(Game1.Network.Role != NetworkRole.Server)
            {
                ApplyState(netState, deltaTime);
            }
        }

        public void ApplyState(ShotNetState state, float deltaTime = 0)
        {
            this.ApplyPositionableNetState(state);
            this.Position.X += (state.VelocityX * deltaTime);
            this.Position.Y += (state.VelocityY * deltaTime);
            shotLifeRemaining = state.ShotLife;
        }

        public void CheckShotDeath()
        {
            if(shotLifeRemaining > 0)
            {
                shotLifeRemaining -= TimeManager.SecondDifference;
            }
            else
            {
                if(Game1.Network.Role == NetworkRole.Server)
                {
                    Game1.Network.RequestDestroyEntity(this);
                }
            }

        }
	}
}
