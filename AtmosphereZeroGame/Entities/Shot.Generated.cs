#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using System.Linq;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using FlatRedBall;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math.Geometry;
using AtmosphereZeroGame.Entities.Effects;
namespace AtmosphereZeroGame.Entities
{
    public partial class Shot : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable, FlatRedBall.Performance.IPoolable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        protected static FlatRedBall.Graphics.Animation.AnimationChainList AnimationChains;
        
        private FlatRedBall.Math.Geometry.Circle mCollisionCircle;
        public FlatRedBall.Math.Geometry.Circle CollisionCircle
        {
            get
            {
                return mCollisionCircle;
            }
            private set
            {
                mCollisionCircle = value;
            }
        }
        private FlatRedBall.Sprite RocketSprite;
        private AtmosphereZeroGame.Entities.Effects.RocketTrail RocketTrailInstance;
        public int Index { get; set; }
        public bool Used { get; set; }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public Shot () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public Shot (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public Shot (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            mCollisionCircle = new FlatRedBall.Math.Geometry.Circle();
            mCollisionCircle.Name = "mCollisionCircle";
            RocketSprite = new FlatRedBall.Sprite();
            RocketSprite.Name = "RocketSprite";
            RocketTrailInstance = new AtmosphereZeroGame.Entities.Effects.RocketTrail(ContentManagerName, false);
            RocketTrailInstance.Name = "RocketTrailInstance";
            
            PostInitialize();
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionCircle, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(RocketSprite, LayerProvidedByContainer);
            RocketTrailInstance.ReAddToManagers(LayerProvidedByContainer);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionCircle, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(RocketSprite, LayerProvidedByContainer);
            RocketTrailInstance.AddToManagers(LayerProvidedByContainer);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            RocketTrailInstance.Activity();
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            if (Used)
            {
                Factories.ShotFactory.MakeUnused(this, false);
            }
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            if (CollisionCircle != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionCircle);
            }
            if (RocketSprite != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(RocketSprite);
            }
            if (RocketTrailInstance != null)
            {
                RocketTrailInstance.Destroy();
                RocketTrailInstance.Detach();
            }
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (mCollisionCircle.Parent == null)
            {
                mCollisionCircle.CopyAbsoluteToRelative();
                mCollisionCircle.AttachTo(this, false);
            }
            CollisionCircle.Radius = 16f;
            CollisionCircle.Visible = false;
            CollisionCircle.Color = Microsoft.Xna.Framework.Color.Red;
            if (RocketSprite.Parent == null)
            {
                RocketSprite.CopyAbsoluteToRelative();
                RocketSprite.AttachTo(this, false);
            }
            if (RocketSprite.Parent == null)
            {
                RocketSprite.Z = -0.1f;
            }
            else
            {
                RocketSprite.RelativeZ = -0.1f;
            }
            RocketSprite.TextureScale = 1f;
            RocketSprite.UseAnimationRelativePosition = false;
            RocketSprite.Animate = false;
            RocketSprite.AnimationChains = AnimationChains;
            RocketSprite.CurrentChainName = "Rocket";
            if (RocketSprite.Parent == null)
            {
                RocketSprite.RotationZ = -1.57f;
            }
            else
            {
                RocketSprite.RelativeRotationZ = -1.57f;
            }
            if (RocketTrailInstance.Parent == null)
            {
                RocketTrailInstance.CopyAbsoluteToRelative();
                RocketTrailInstance.AttachTo(this, false);
            }
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            if (CollisionCircle != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionCircle);
            }
            if (RocketSprite != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(RocketSprite);
            }
            RocketTrailInstance.RemoveFromManagers();
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
                RocketTrailInstance.AssignCustomVariables(true);
            }
            CollisionCircle.Radius = 16f;
            CollisionCircle.Visible = false;
            CollisionCircle.Color = Microsoft.Xna.Framework.Color.Red;
            if (RocketSprite.Parent == null)
            {
                RocketSprite.Z = -0.1f;
            }
            else
            {
                RocketSprite.RelativeZ = -0.1f;
            }
            RocketSprite.TextureScale = 1f;
            RocketSprite.UseAnimationRelativePosition = false;
            RocketSprite.Animate = false;
            RocketSprite.AnimationChains = AnimationChains;
            RocketSprite.CurrentChainName = "Rocket";
            if (RocketSprite.Parent == null)
            {
                RocketSprite.RotationZ = -1.57f;
            }
            else
            {
                RocketSprite.RelativeRotationZ = -1.57f;
            }
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(RocketSprite);
            RocketTrailInstance.ConvertToManuallyUpdated();
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("ShotStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/globalcontent/animationchains.achx", ContentManagerName))
                {
                    registerUnload = true;
                }
                AnimationChains = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/globalcontent/animationchains.achx", ContentManagerName);
            }
            AtmosphereZeroGame.Entities.Effects.RocketTrail.LoadStaticContent(contentManagerName);
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("ShotStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
                if (AnimationChains != null)
                {
                    AnimationChains= null;
                }
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "AnimationChains":
                    return AnimationChains;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "AnimationChains":
                    return AnimationChains;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "AnimationChains":
                    return AnimationChains;
            }
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(CollisionCircle);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(RocketSprite);
            RocketTrailInstance.SetToIgnorePausing();
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(CollisionCircle);
            }
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(CollisionCircle, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(RocketSprite);
            }
            FlatRedBall.SpriteManager.AddToLayer(RocketSprite, layerToMoveTo);
            RocketTrailInstance.MoveToLayer(layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
