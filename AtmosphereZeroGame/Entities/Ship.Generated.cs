#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using System.Linq;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using FlatRedBall;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math.Geometry;
using AtmosphereZeroGame.Entities.Effects;
namespace AtmosphereZeroGame.Entities
{
    public partial class Ship : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable, FlatRedBall.Performance.IPoolable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        protected static FlatRedBall.Graphics.Animation.AnimationChainList AnimationChains;
        
        private FlatRedBall.Math.Geometry.Circle mCollisionCircle;
        public FlatRedBall.Math.Geometry.Circle CollisionCircle
        {
            get
            {
                return mCollisionCircle;
            }
            private set
            {
                mCollisionCircle = value;
            }
        }
        private FlatRedBall.Sprite ShipSprite;
        private AtmosphereZeroGame.Entities.Effects.EngineTrail EngineTrailInstance;
        public int Index { get; set; }
        public bool Used { get; set; }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public Ship () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public Ship (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public Ship (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            mCollisionCircle = new FlatRedBall.Math.Geometry.Circle();
            mCollisionCircle.Name = "mCollisionCircle";
            ShipSprite = new FlatRedBall.Sprite();
            ShipSprite.Name = "ShipSprite";
            EngineTrailInstance = new AtmosphereZeroGame.Entities.Effects.EngineTrail(ContentManagerName, false);
            EngineTrailInstance.Name = "EngineTrailInstance";
            
            PostInitialize();
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionCircle, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(ShipSprite, LayerProvidedByContainer);
            EngineTrailInstance.ReAddToManagers(LayerProvidedByContainer);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionCircle, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(ShipSprite, LayerProvidedByContainer);
            EngineTrailInstance.AddToManagers(LayerProvidedByContainer);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            EngineTrailInstance.Activity();
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            if (Used)
            {
                Factories.ShipFactory.MakeUnused(this, false);
            }
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            if (CollisionCircle != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionCircle);
            }
            if (ShipSprite != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(ShipSprite);
            }
            if (EngineTrailInstance != null)
            {
                EngineTrailInstance.Destroy();
                EngineTrailInstance.Detach();
            }
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (mCollisionCircle.Parent == null)
            {
                mCollisionCircle.CopyAbsoluteToRelative();
                mCollisionCircle.AttachTo(this, false);
            }
            CollisionCircle.Radius = 32f;
            CollisionCircle.Visible = false;
            CollisionCircle.Color = Microsoft.Xna.Framework.Color.YellowGreen;
            if (ShipSprite.Parent == null)
            {
                ShipSprite.CopyAbsoluteToRelative();
                ShipSprite.AttachTo(this, false);
            }
            ShipSprite.TextureScale = 1f;
            ShipSprite.UseAnimationRelativePosition = false;
            ShipSprite.Animate = false;
            ShipSprite.AnimationChains = AnimationChains;
            ShipSprite.CurrentChainName = "Ships";
            if (ShipSprite.Parent == null)
            {
                ShipSprite.RotationZ = -1.57f;
            }
            else
            {
                ShipSprite.RelativeRotationZ = -1.57f;
            }
            if (EngineTrailInstance.Parent == null)
            {
                EngineTrailInstance.CopyAbsoluteToRelative();
                EngineTrailInstance.AttachTo(this, false);
            }
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            if (CollisionCircle != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionCircle);
            }
            if (ShipSprite != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(ShipSprite);
            }
            EngineTrailInstance.RemoveFromManagers();
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
                EngineTrailInstance.AssignCustomVariables(true);
            }
            CollisionCircle.Radius = 32f;
            CollisionCircle.Visible = false;
            CollisionCircle.Color = Microsoft.Xna.Framework.Color.YellowGreen;
            ShipSprite.TextureScale = 1f;
            ShipSprite.UseAnimationRelativePosition = false;
            ShipSprite.Animate = false;
            ShipSprite.AnimationChains = AnimationChains;
            ShipSprite.CurrentChainName = "Ships";
            if (ShipSprite.Parent == null)
            {
                ShipSprite.RotationZ = -1.57f;
            }
            else
            {
                ShipSprite.RelativeRotationZ = -1.57f;
            }
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(ShipSprite);
            EngineTrailInstance.ConvertToManuallyUpdated();
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("ShipStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/globalcontent/animationchains.achx", ContentManagerName))
                {
                    registerUnload = true;
                }
                AnimationChains = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/globalcontent/animationchains.achx", ContentManagerName);
            }
            AtmosphereZeroGame.Entities.Effects.EngineTrail.LoadStaticContent(contentManagerName);
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("ShipStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
                if (AnimationChains != null)
                {
                    AnimationChains= null;
                }
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "AnimationChains":
                    return AnimationChains;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "AnimationChains":
                    return AnimationChains;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "AnimationChains":
                    return AnimationChains;
            }
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(CollisionCircle);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(ShipSprite);
            EngineTrailInstance.SetToIgnorePausing();
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(CollisionCircle);
            }
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(CollisionCircle, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(ShipSprite);
            }
            FlatRedBall.SpriteManager.AddToLayer(ShipSprite, layerToMoveTo);
            EngineTrailInstance.MoveToLayer(layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
