using AtmosphereZeroGame.Input;
using AtmosphereZeroGame.NetStates;
using AtmosphereZeroGame.Utilities;
using FlatRedBall;
using RedGrin;
using System;

namespace AtmosphereZeroGame.Entities
{
    public partial class Ship : INetworkEntity
    {
        // TODO: replace this once we have real models!
        private const float MaxAcceleration = 350f;
        private const float ShotsPerSecond = 2f;
        private const float ShipDrag = 0.5f;
        public const float ShipMaxHealth = 100f;

        public long OwnerId { get; set; }
        public long EntityId { get; set; }
        public IShipInput Input { get; set; }
        public bool Firing { get; set; } = false;
        public float Health
        {
            get
            {
                return currentHealth;
            }
        }

        private float ReloadSeconds
        {
            get
            {
                return 1f / ShotsPerSecond;
            }
        }
        private ShipNetState netState = new ShipNetState();
        private float secondsToReload = 0;
        private float currentHealth = ShipMaxHealth;

        private void CustomInitialize()
        {
            Drag = ShipDrag;

        }

        private void CustomActivity()
        {
            ApplyInput();
            DoFiring();
            DoTrails();
        }

        private void CustomDestroy()
        {


        }

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        public object GetState()
        {
            // apply basic positionable properties
            this.CopyToState(netState);

            // apply ship properties
            netState.Firing = Firing;
            netState.Health = currentHealth;

            // return the state
            return netState;
        }

        public void UpdateState(object entityState, double stateTime, bool isReckoningState = false)
        {
            netState = (ShipNetState)entityState;
            var deltaTime = (float)(Game1.Network.ServerTime - stateTime);

            if (OwnerId != Game1.Network.NetworkId || isReckoningState)
            {
                ApplyState(netState, deltaTime);
            }
        }

        public void ApplyState(ShipNetState state, float deltaTime = 0)
        {
            // apply basic state
            this.ApplyPositionableNetState(state);

            // apply some predictive physics
            this.Position.X += (state.VelocityX * deltaTime);
            this.Position.Y += (state.VelocityY * deltaTime);

            // apply ship-specific state stuff
            Firing = state.Firing;
            currentHealth = state.Health;
            ShipSprite.CurrentFrameIndex = state.SpriteFrame;
        }

        public void ApplyInput()
        {
            // EARLY OUT: null input
            if (Input == null)
            {
                return;
            }

            var rotation = Input.Direction.Magnitude > 0 ? (float)Math.Atan2(Input.Direction.Y, Input.Direction.X) : RotationZ;
            rotation = FlatRedBall.Math.MathFunctions.RegulateAngle(rotation);
            var throttle = MaxAcceleration * Input.Direction.Magnitude;
            var accelX = (float)(Math.Cos(rotation) * throttle);
            var accelY = (float)(Math.Sin(rotation) * throttle);
            bool requestUpdate = false;

            if (Acceleration.X != accelX || Acceleration.Y != accelY || RotationZ != rotation || Firing != Input.Firing.IsDown)
            {
                Acceleration.X = accelX;
                Acceleration.Y = accelY;
                RotationZ = rotation;
                Firing = Input.Firing.IsDown;
                requestUpdate = true;
            }

            if (Game1.Network.NetworkId == OwnerId && requestUpdate)
            {
                Game1.Network.RequestUpdateEntity(this);
            }
        }

        public void ApplyDamage(float amount)
        {
            currentHealth -= amount;

            if (Game1.Network.Role == NetworkRole.Server)
            {
                Game1.Network.RequestUpdateEntity(this);
            }
        }

        private void DoFiring()
        {
            // We only handle the firing routine if we are the owner of this ship.
            // Otherwise every client will request shots be created.
            if (Game1.Network.NetworkId == OwnerId)
            {
                if (secondsToReload > 0)
                {
                    secondsToReload -= TimeManager.SecondDifference;
                }

                if (Firing && secondsToReload <= 0)
                {
                    var shotState = new ShotNetState()
                    {
                        PositionX = this.Position.X,
                        PositionY = this.Position.Y,
                        VelocityX = (float)(Math.Cos(RotationZ) * Shot.ShotSpeed),
                        VelocityY = (float)(Math.Sin(RotationZ) * Shot.ShotSpeed),
                        Rotation = this.RotationZ,
                        ShotLife = Shot.ShotLifeSeconds
                    };
                    Game1.Network.RequestCreateEntity(shotState);

                    secondsToReload = ReloadSeconds;
                }
            }
        }

        private void DoTrails()
        {
            EngineTrailInstance.Emitting = Acceleration.Length() > 0;
        }
    }
}
