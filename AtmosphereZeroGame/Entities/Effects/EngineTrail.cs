using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;

namespace AtmosphereZeroGame.Entities.Effects
{
	public partial class EngineTrail
	{
        public bool Emitting
        {
            get
            {
                return TrailEmitter.TimedEmission;
            }
            set
            {
                TrailEmitter.TimedEmission = value;
            }
        }

		private void CustomInitialize()
		{
            SetUpEmitter();

		}

		private void CustomActivity()
		{


		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        private void SetUpEmitter()
        {
            TrailEmitter.EmissionSettings = new EmissionSettings()
            {
                Alpha = 1,
                AlphaRate = -0.5f,
                MatchScaleXToY = true,
                ScaleY = 12,
                RotationZ = -3.14f,
                RotationZRange = 6.28f,
                RotationZVelocity = -2f,
                RotationZVelocityRange = 8f,
                ScaleYVelocity = 6,
                ScaleYVelocityRange = 8,
                AnimationChain = GlobalContent.AnimationChains["EngineGoo"],
                
                VelocityRangeType = RangeType.Wedge,
                WedgeAngle = 3.14f,
                WedgeSpread = 1.54f,
                RadialVelocity = 5f,
                RadialVelocityRange = 50f,
                Animate = false
            };

            TrailEmitter.RelativePosition.Z = 0.2f;
            TrailEmitter.SecondFrequency = 0.05f;
            TrailEmitter.RelativePosition.X = -38f;
            TrailEmitter.NumberPerEmission = 2;
            TrailEmitter.RemovalEvent = Emitter.RemovalEventType.Alpha0;
        }
    }
}
