﻿using AtmosphereZeroGame.NetStates;
using FlatRedBall;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmosphereZeroGame.Utilities
{
    public static class PositionedObjectExtensions
    {
        public static void ApplyPositionableNetState(this PositionedObject po, PositionableNetState state)
        {
            po.Acceleration.X = state.AccelerationX;
            po.Acceleration.Y = state.AccelerationY;
            po.Velocity.X = state.VelocityX;
            po.Velocity.Y = state.VelocityY;
            po.Position.X = state.PositionX;
            po.Position.Y = state.PositionY;
            po.RotationZ = state.Rotation;
        }

        public static PositionableNetState CopyToState(this PositionedObject po, PositionableNetState state = null)
        {
            if(state == null)
            {
                state = new PositionableNetState();
            }

            state.AccelerationX = po.Acceleration.X;
            state.AccelerationY = po.Acceleration.Y;
            state.VelocityX = po.Velocity.X;
            state.VelocityY = po.Velocity.Y;
            state.PositionX = po.Position.X;
            state.PositionY = po.Position.Y;
            state.Rotation = po.RotationZ;

            return state;
        }
    }
}
