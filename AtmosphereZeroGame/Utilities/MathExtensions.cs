﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmosphereZeroGame.Utilities
{
    public static class MathExtensions
    {
        public static int Clamp(this int value, int min, int max)
        {
            var returnValue = value;
            returnValue = Math.Min(returnValue, max);
            returnValue = Math.Max(returnValue, min);

            return returnValue;
        }
    }
}
