﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmosphereZeroGame.NetStates
{
    public class ShotNetState : PositionableNetState
    {
        public float ShotLife { get; set; }
    }
}
