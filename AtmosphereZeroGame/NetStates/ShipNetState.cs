﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmosphereZeroGame.NetStates
{
    public class ShipNetState : PositionableNetState
    {
        public bool Firing { get; set; }
        public float Health { get; set; }
        public int SpriteFrame { get; set; }
    }
}
