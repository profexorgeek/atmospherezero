﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmosphereZeroGame.NetStates
{
    public class PositionableNetState
    {
        public float AccelerationX { get; set; }
        public float AccelerationY { get; set; }
        public float VelocityX { get; set; }
        public float VelocityY { get; set; }
        public float Rotation { get; set; }
        public float PositionX { get; set; }
        public float PositionY { get; set; }
    }
}
