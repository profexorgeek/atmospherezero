﻿using RedGrin.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmosphereZeroGame.Logging
{
    public class Log : ILogger
    {
        const int LogCount = 500;

        private static Log instance;
        public static Log Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new Log();
                }
                return instance;
            }
        }

        private List<string> Logs;

        private Log() {
            FlatRedBall.Debugging.Debugger.TextCorner = FlatRedBall.Debugging.Debugger.Corner.BottomRight;
            Logs = new List<string>();
        }


        public LogLevels Level { get; set; } = LogLevels.Debug;

        public void Debug(string message)
        {
            if(Level <= LogLevels.Debug)
            {
                Write($"DEBUG: {message}");
            }
        }

        public void Info(string message)
        {
            if (Level <= LogLevels.Info)
            {
                Write($"INFO: {message}");
            }
        }

        public void Warning(string message)
        {
            if (Level <= LogLevels.Warning)
            {
                Write($"WARN: {message}");
            }
        }

        public void Error(string message)
        {
            if (Level <= LogLevels.Error)
            {
                Write($"ERROR: {message}");
            }
        }

        public void Write(string message)
        {
            Logs.Add(message);
            if(Logs.Count > LogCount)
            {
                Logs.RemoveAt(0);
            }

            FlatRedBall.Debugging.Debugger.CommandLineWrite(message);
        }
    }
}
