using AtmosphereZeroGame.Entities.Effects;
using System;
using FlatRedBall.Math;
using FlatRedBall.Graphics;
using AtmosphereZeroGame.Performance;

namespace AtmosphereZeroGame.Factories
{
    public class EngineTrailFactory : IEntityFactory
    {
        public static FlatRedBall.Math.Axis? SortAxis { get; set;}
        public static EngineTrail CreateNew (float x = 0, float y = 0) 
        {
            return CreateNew(null, x, y);
        }
        public static EngineTrail CreateNew (Layer layer, float x = 0, float y = 0) 
        {
            EngineTrail instance = null;
            instance = new EngineTrail(mContentManagerName ?? FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, false);
            instance.AddToManagers(layer);
            instance.X = x;
            instance.Y = y;
            foreach (var list in ListsToAddTo)
            {
                if (SortAxis == FlatRedBall.Math.Axis.X && list is PositionedObjectList<EngineTrail>)
                {
                    var index = (list as PositionedObjectList<EngineTrail>).GetFirstAfter(x, Axis.X, 0, list.Count);
                    list.Insert(index, instance);
                }
                else if (SortAxis == FlatRedBall.Math.Axis.Y && list is PositionedObjectList<EngineTrail>)
                {
                    var index = (list as PositionedObjectList<EngineTrail>).GetFirstAfter(y, Axis.Y, 0, list.Count);
                    list.Insert(index, instance);
                }
                else
                {
                    // Sort Z not supported
                    list.Add(instance);
                }
            }
            if (EntitySpawned != null)
            {
                EntitySpawned(instance);
            }
            return instance;
        }
        
        public static void Initialize (string contentManager) 
        {
            mContentManagerName = contentManager;
        }
        
        public static void Destroy () 
        {
            mContentManagerName = null;
            ListsToAddTo.Clear();
            SortAxis = null;
            mPool.Clear();
            EntitySpawned = null;
        }
        
        private static void FactoryInitialize () 
        {
            const int numberToPreAllocate = 20;
            for (int i = 0; i < numberToPreAllocate; i++)
            {
                EngineTrail instance = new EngineTrail(mContentManagerName, false);
                mPool.AddToPool(instance);
            }
        }
        
        /// <summary>
        /// Makes the argument objectToMakeUnused marked as unused.  This method is generated to be used
        /// by generated code.  Use Destroy instead when writing custom code so that your code will behave
        /// the same whether your Entity is pooled or not.
        /// </summary>
        public static void MakeUnused (EngineTrail objectToMakeUnused) 
        {
            MakeUnused(objectToMakeUnused, true);
        }
        
        /// <summary>
        /// Makes the argument objectToMakeUnused marked as unused.  This method is generated to be used
        /// by generated code.  Use Destroy instead when writing custom code so that your code will behave
        /// the same whether your Entity is pooled or not.
        /// </summary>
        public static void MakeUnused (EngineTrail objectToMakeUnused, bool callDestroy) 
        {
            if (callDestroy)
            {
                objectToMakeUnused.Destroy();
            }
        }
        
        public static void AddList<T> (System.Collections.Generic.IList<T> newList) where T : EngineTrail
        {
            ListsToAddTo.Add(newList as System.Collections.IList);
        }
        public static void RemoveList<T> (System.Collections.Generic.IList<T> newList) where T : EngineTrail
        {
            ListsToAddTo.Remove(newList as System.Collections.IList);
        }
        public static void ClearListsToAddTo () 
        {
            ListsToAddTo.Clear();
        }
        
        
            static string mContentManagerName;
            static System.Collections.Generic.List<System.Collections.IList> ListsToAddTo = new System.Collections.Generic.List<System.Collections.IList>();
            static PoolList<EngineTrail> mPool = new PoolList<EngineTrail>();
            public static Action<EngineTrail> EntitySpawned;
            object IEntityFactory.CreateNew () 
            {
                return EngineTrailFactory.CreateNew();
            }
            object IEntityFactory.CreateNew (Layer layer) 
            {
                return EngineTrailFactory.CreateNew(layer);
            }
            void IEntityFactory.Initialize (string contentManagerName) 
            {
                EngineTrailFactory.Initialize(contentManagerName);
            }
            void IEntityFactory.ClearListsToAddTo () 
            {
                EngineTrailFactory.ClearListsToAddTo();
            }
            static EngineTrailFactory mSelf;
            public static EngineTrailFactory Self
            {
                get
                {
                    if (mSelf == null)
                    {
                        mSelf = new EngineTrailFactory();
                    }
                    return mSelf;
                }
            }
    }
}
