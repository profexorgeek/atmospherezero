﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmosphereZeroGame.Templates
{
    public class ShipTemplate
    {
        public string Name { get; set; }
        public float Hull { get; set; }
        public int Storage { get; set; }

    }
}
