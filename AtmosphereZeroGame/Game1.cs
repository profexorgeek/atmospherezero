using System;
using System.Collections.Generic;
using System.Reflection;

using FlatRedBall;
using FlatRedBall.Graphics;
using FlatRedBall.Screens;
using Microsoft.Xna.Framework;

using System.Linq;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RedGrin;

namespace AtmosphereZeroGame
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public static NetworkManager Network { get; set; }
        GraphicsDeviceManager graphics;

        public Game1() : base()
        {
            graphics = new GraphicsDeviceManager(this);

#if WINDOWS_PHONE || ANDROID || IOS

            // Frame rate is 30 fps by default for Windows Phone,
            // so let's keep that for other phones too
            TargetElapsedTime = TimeSpan.FromTicks(333333);
            graphics.IsFullScreen = true;
#elif WINDOWS || DESKTOP_GL
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;
#endif


#if WINDOWS_8
            FlatRedBall.Instructions.Reflection.PropertyValuePair.TopLevelAssembly = 
                this.GetType().GetTypeInfo().Assembly;
#endif

        }

        protected override void Initialize()
        {
            #if IOS
            var bounds = UIKit.UIScreen.MainScreen.Bounds;
            var nativeScale = UIKit.UIScreen.MainScreen.Scale;
            var screenWidth = (int)(bounds.Width * nativeScale);
            var screenHeight = (int)(bounds.Height * nativeScale);
            graphics.PreferredBackBufferWidth = screenWidth;
            graphics.PreferredBackBufferHeight = screenHeight;
            #endif
        
            FlatRedBallServices.InitializeFlatRedBall(this, graphics);

            InitializeCamera();

            InitializeNetwork();

			GlobalContent.Initialize();

			FlatRedBall.Screens.ScreenManager.Start(typeof(AtmosphereZeroGame.Screens.Title));

            base.Initialize();
        }

        private void InitializeCamera()
        {
            CameraSetup.SetupCamera(SpriteManager.Camera, graphics);
            Camera.Main.UsePixelCoordinates3D(0);
            Camera.Main.FarClipPlane = 999999;
            IsMouseVisible = true;
        }

        private void InitializeNetwork()
        {
            Network = new NetworkManager(Configuration.General.NetConfig, Logging.Log.Instance);
        }


        protected override void Update(GameTime gameTime)
        {
            FlatRedBallServices.Update(gameTime);

            FlatRedBall.Screens.ScreenManager.Activity();

            if(Network != null)
            {
                Network.Update();
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            FlatRedBallServices.Draw();

            base.Draw(gameTime);
        }
    }
}
