#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using System.Linq;
using FlatRedBall;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math;
namespace AtmosphereZeroGame.Screens
{
    public partial class Arena : FlatRedBall.Screens.Screen
    {
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        
        private FlatRedBall.Math.PositionedObjectList<AtmosphereZeroGame.Entities.Ship> Ships;
        private FlatRedBall.Math.PositionedObjectList<AtmosphereZeroGame.Entities.Star> Stars;
        private FlatRedBall.Math.PositionedObjectList<AtmosphereZeroGame.Entities.Shot> Shots;
        public Arena () 
        	: base ("Arena")
        {
        }
        public override void Initialize (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            Ships = new FlatRedBall.Math.PositionedObjectList<AtmosphereZeroGame.Entities.Ship>();
            Ships.Name = "Ships";
            Stars = new FlatRedBall.Math.PositionedObjectList<AtmosphereZeroGame.Entities.Star>();
            Stars.Name = "Stars";
            Shots = new FlatRedBall.Math.PositionedObjectList<AtmosphereZeroGame.Entities.Shot>();
            Shots.Name = "Shots";
            
            
            PostInitialize();
            base.Initialize(addToManagers);
            if (addToManagers)
            {
                AddToManagers();
            }
        }
        public override void AddToManagers () 
        {
            Factories.ShipFactory.Initialize(ContentManagerName);
            Factories.StarFactory.Initialize(ContentManagerName);
            Factories.ShotFactory.Initialize(ContentManagerName);
            Factories.ShipFactory.AddList(Ships);
            Factories.StarFactory.AddList(Stars);
            Factories.ShotFactory.AddList(Shots);
            base.AddToManagers();
            AddToManagersBottomUp();
            CustomInitialize();
        }
        public override void Activity (bool firstTimeCalled) 
        {
            if (!IsPaused)
            {
                
                for (int i = Ships.Count - 1; i > -1; i--)
                {
                    if (i < Ships.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        Ships[i].Activity();
                    }
                }
                for (int i = Stars.Count - 1; i > -1; i--)
                {
                    if (i < Stars.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        Stars[i].Activity();
                    }
                }
                for (int i = Shots.Count - 1; i > -1; i--)
                {
                    if (i < Shots.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        Shots[i].Activity();
                    }
                }
            }
            else
            {
            }
            base.Activity(firstTimeCalled);
            if (!IsActivityFinished)
            {
                CustomActivity(firstTimeCalled);
            }
        }
        public override void Destroy () 
        {
            base.Destroy();
            Factories.ShipFactory.Destroy();
            Factories.StarFactory.Destroy();
            Factories.ShotFactory.Destroy();
            
            Ships.MakeOneWay();
            Stars.MakeOneWay();
            Shots.MakeOneWay();
            for (int i = Ships.Count - 1; i > -1; i--)
            {
                Ships[i].Destroy();
            }
            for (int i = Stars.Count - 1; i > -1; i--)
            {
                Stars[i].Destroy();
            }
            for (int i = Shots.Count - 1; i > -1; i--)
            {
                Shots[i].Destroy();
            }
            Ships.MakeTwoWay();
            Stars.MakeTwoWay();
            Shots.MakeTwoWay();
            FlatRedBall.Math.Collision.CollisionManager.Self.Relationships.Clear();
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp () 
        {
            CameraSetup.ResetCamera(SpriteManager.Camera);
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            for (int i = Ships.Count - 1; i > -1; i--)
            {
                Ships[i].Destroy();
            }
            for (int i = Stars.Count - 1; i > -1; i--)
            {
                Stars[i].Destroy();
            }
            for (int i = Shots.Count - 1; i > -1; i--)
            {
                Shots[i].Destroy();
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            for (int i = 0; i < Ships.Count; i++)
            {
                Ships[i].ConvertToManuallyUpdated();
            }
            for (int i = 0; i < Stars.Count; i++)
            {
                Stars[i].ConvertToManuallyUpdated();
            }
            for (int i = 0; i < Shots.Count; i++)
            {
                Shots[i].ConvertToManuallyUpdated();
            }
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            CustomLoadStaticContent(contentManagerName);
        }
        public override void PauseThisScreen () 
        {
            StateInterpolationPlugin.TweenerManager.Self.Pause();
            base.PauseThisScreen();
        }
        public override void UnpauseThisScreen () 
        {
            StateInterpolationPlugin.TweenerManager.Self.Unpause();
            base.UnpauseThisScreen();
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            return null;
        }
        public static object GetFile (string memberName) 
        {
            return null;
        }
        object GetMember (string memberName) 
        {
            return null;
        }
    }
}
