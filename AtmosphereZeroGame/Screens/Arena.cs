using AtmosphereZeroGame.Entities;
using AtmosphereZeroGame.Factories;
using AtmosphereZeroGame.Input;
using AtmosphereZeroGame.Logging;
using AtmosphereZeroGame.NetStates;
using FlatRedBall;
using FlatRedBall.Gui;
using FlatRedBall.Input;
using Microsoft.Xna.Framework;
using RedGrin;
using System;
using System.Linq;
using Narfox.FRBHelpers;

namespace AtmosphereZeroGame.Screens
{
    public partial class Arena : INetworkArena
    {
        // TODO: move these to a config class
        const int NumberOfStars = 200;
        const float WorldSize = 2000;
        const float MaxZDepth = -1000;
        const float MinZDepth = 50f;
        const float RespawnSeconds = 5f;

        private Ship player;
        private float secondsToRespawn = 0;
        private bool firstSpawnComplete = false;
        private bool spawnInProgress = false;
        private int shipSpriteFrame = 0;




        public Ship Player
        {
            get
            {
                return player;
            }
            set
            {
                Camera.Main.Detach();

                if(player != null && value != null)
                {
                    throw new Exception("Trying to set a new player ship when a player already exists.");
                }

                player = value;
                if (player != null)
                {
                    firstSpawnComplete = true;
                    Camera.Main.AttachTo(player, true);
                    Camera.Main.RelativePosition.X = 0;
                    Camera.Main.RelativePosition.Y = 0;
                    Player.Input = new KeyboardInput();
                    spawnInProgress = false;
                }
                else
                {
                    // player must have just died, reset respawn
                    secondsToRespawn = RespawnSeconds;
                }
            }
        }

        private void CustomInitialize()
        {
            Camera.Main.BackgroundColor = new Color(30, 30, 30);
            Camera.Main.ParentRotationChangesRotation = false;
            Game1.Network.GameArena = this;

            // choose a player ship color to persist across deaths
            shipSpriteFrame = FlatRedBall.FlatRedBallServices.Random.Next(0, GlobalContent.AnimationChains["Ships"].Count);

            Game1.Network.Connected += (id, data) =>
            {
                Log.Instance.Info($"Connection message from: {id}");
                CreateLocalPlayer();
            };

            Game1.Network.Disconnected += (id, data) =>
            {
                Log.Instance.Info($"Disconnection message from: {id}");
                MoveToScreen(typeof(Title));
            };

            if(Game1.Network.Role == NetworkRole.Server)
            {
                CreateLocalPlayer();
            }

            CreateStars();
        }

        private void CustomActivity(bool firstTimeCalled)
        {
            CollideShips();
            CollideShots();
            ManageShips();
            TrySpawnPlayer();
        }

        private void CustomDestroy()
        {


        }

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }


        #region INetworkArena
        public INetworkEntity RequestCreateEntity(long ownerId, long entityId, object entityData)
        {
            INetworkEntity entity = null;

            if (entityData is ShipNetState)
            {
                entity = CreateShip(ownerId, entityId, (ShipNetState)entityData);
            }
            else if(entityData is ShotNetState)
            {
                entity = CreateShot(ownerId, entityId, (ShotNetState)entityData);
            }

            return entity;
        }

        public void RequestDestroyEntity(INetworkEntity entity)
        {
            if(entity is Ship)
            {
                DestroyShip((Ship)entity);
            }

            if(entity is Shot)
            {
                DestroyShot((Shot)entity);
            }
        }
        #endregion


        private void CreateLocalPlayer()
        {
            var posX = FlatRedBallServices.Random.FloatInRange(-WorldSize / 2f, WorldSize / 2f);
            var posY = FlatRedBallServices.Random.FloatInRange(-WorldSize / 2f, WorldSize / 2f);

            if (Player == null)
            {
                var state = new ShipNetState()
                {
                    PositionX = posX,
                    PositionY = posY,
                    Health = Ship.ShipMaxHealth,
                    SpriteFrame = shipSpriteFrame
                };
                spawnInProgress = true;

                Game1.Network.RequestCreateEntity(state);
            }
        }

        private INetworkEntity CreateShip(long ownerId, long entityId, ShipNetState entityData)
        {
            var ship = ShipFactory.CreateNew();
            ship.OwnerId = ownerId;
            ship.EntityId = entityId;
            ship.ApplyState(entityData);

            // if the owner ID matches our network ID this is the player ship!
            if (ownerId == Game1.Network.NetworkId)
            {
                Player = ship;
            }

            return ship;
        }

        private void DestroyShip(Ship ship)
        {
            var target = Ships.Where(s => s.EntityId == ship.EntityId).FirstOrDefault();
            if (target == Player)
            {
                target.Input = null;
                Player = null;
            }
            target?.Destroy();
        }

        private INetworkEntity CreateShot(long ownerId, long entityId, ShotNetState entityData)
        {
            var shot = ShotFactory.CreateNew();
            shot.OwnerId = ownerId;
            shot.EntityId = entityId;
            shot.ApplyState(entityData);

            Log.Instance.Debug($"Created shot #{Shots.Count}");

            return shot;
        }

        private void DestroyShot(Shot shot)
        {
            Shots.Where(s => s.EntityId == shot.EntityId).FirstOrDefault()?.Destroy();
        }



        private void CreateStars()
        {
            var rand = FlatRedBallServices.Random;
            var halfWorldSize = (WorldSize / 2f);

            for (var i = 0; i < NumberOfStars; i++)
            {
                var star = StarFactory.CreateNew();
                star.Position.X = rand.FloatInRange(-halfWorldSize, halfWorldSize);
                star.Position.Y = rand.FloatInRange(-halfWorldSize, halfWorldSize);
                star.Position.Z = rand.FloatInRange(MaxZDepth, MinZDepth);
                star.RotationZ = rand.FloatInRange(-Math.PI, Math.PI);
            }
        }

        private void CollideShips()
        {
            for(var i = Ships.Count -1; i > -1; i--)
            {
                for(var j = Ships.Count - 1; j > -1; j--)
                {
                    if(Ships[i] != Ships[j])
                    {
                        Ships[i].CollisionCircle.CollideAgainstBounce(Ships[j].CollisionCircle, 0.5f, 0.5f, 0.5f);
                    }
                }
            }
        }

        private void CollideShots()
        {
            // collide shots vs ships
            for(var i = Shots.Count - 1; i > -1; i--)
            {
                var shot = Shots[i];
                bool shotCollided = false;

                for(var j = Ships.Count - 1; j > -1; j--)
                {
                    var ship = Ships[j];

                    if(!shotCollided && 
                        shot.OwnerId != ship.OwnerId && 
                        shot.CollisionCircle.CollideAgainstBounce(ship.CollisionCircle, 0.1f, 0.9f, 0.5f))
                    {
                        if (Game1.Network.Role == NetworkRole.Server)
                        {
                            shotCollided = true;
                            ship.ApplyDamage(Shot.ShotDamage);
                        }
                    }
                }

                if (shotCollided)
                {
                    Game1.Network.RequestDestroyEntity(shot);
                }
            }
        }

        private void ManageShips()
        {
            for(var i = Ships.Count - 1; i > -1; i--)
            {
                var ship = Ships[i];

                if(Game1.Network.Role == NetworkRole.Server)
                {
                    if(ship.Health <= 0)
                    {
                        Game1.Network.RequestDestroyEntity(ship);
                    }
                }
            }
        }

        private void TrySpawnPlayer()
        {
            if(firstSpawnComplete && !spawnInProgress && Player == null)
            {
                if(secondsToRespawn <= 0)
                {
                    CreateLocalPlayer();
                }
                else
                {
                    secondsToRespawn -= TimeManager.SecondDifference;
                }
            }
        }
    }
}
