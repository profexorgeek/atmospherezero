using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Configuration;

using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Localization;
using RedGrin;
using AtmosphereZeroGame.Logging;
using Microsoft.Xna.Framework;

namespace AtmosphereZeroGame.Screens
{
	public partial class Title
	{
		void CustomInitialize()
		{
            Camera.Main.BackgroundColor = Color.Gray;
            Log.Instance.Info("Welcome to AtmosphereZero...");
            Log.Instance.Info("Press S to start game as Server.");
            Log.Instance.Info("Press C to start game as Client.");
        }

		void CustomActivity(bool firstTimeCalled)
		{
            bool startGame = false;

            if(InputManager.Keyboard.KeyReleased(Microsoft.Xna.Framework.Input.Keys.C))
            {
                var ip = ConfigurationManager.AppSettings["ServerIp"];

                Game1.Network.Initialize(NetworkRole.Client);
                Game1.Network.Connect(ip);
                startGame = true;
            }
            else if(InputManager.Keyboard.KeyReleased(Microsoft.Xna.Framework.Input.Keys.S))
            {
                Game1.Network.Initialize(RedGrin.NetworkRole.Server);
                startGame = true;
            }

            if(startGame)
            {
                MoveToScreen(typeof(Screens.Arena));
            }

		}

		void CustomDestroy()
		{


		}

        static void CustomLoadStaticContent(string contentManagerName)
        {


        }

	}
}
