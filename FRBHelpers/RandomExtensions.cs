﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Narfox.FRBHelpers
{
    public static class RandomExtensions
    {
        public static float FloatInRange(this Random random, double min, double max)
        {
            return FloatInRange(random, (float)min, (float)max);
        }
        public static float FloatInRange(this Random rand, float min, float max)
        {
            // early out for equal. This may not be perfectly accurate
            // but it makes no difference, all it's doing is saving
            // a calculation
            if (min == max)
            {
                return max;
            }

            var range = max - min;
            var randInRange = (float)(rand.NextDouble() * range);
            return min + randInRange;
        }

        public static T Enumerable<T>(this Random rand, IEnumerable<T> enumerable)
        {
            T o;
            var c = enumerable.Count();
            if (c > 0)
            {
                o = enumerable.ElementAt(rand.Next(0, c));
            }
            else
            {
                o = default(T);
            }

            return o;
        }

        public static T Array<T>(this Random rand, Array array)
        {
            T o;
            var c = array.Length;
            if (c > 0)
            {
                try
                {
                    o = (T)array.GetValue(rand.Next(0, c));
                }
                catch
                {
                    o = default(T);
                }
            }
            else
            {
                o = default(T);
            }

            return o;
        }

        public static byte ColorChannel(this Random rand, byte minTint = 0, byte maxTint = 255)
        {
            return (byte)rand.Next(minTint, maxTint);
        }

        public static T EnumValue<T>(this Random rand)
        {
            Array vals = Enum.GetValues(typeof(T));
            return rand.Array<T>(vals);
        }

        public static bool Boolean(this Random rand)
        {
            if (rand.NextDouble() < 0.5f)
            {
                return true;
            }
            return false;
        }
    }
}
